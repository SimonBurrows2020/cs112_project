/* we can use an arraylist to store cards value
and use loops to sort the value for sepuence.

After that we can create componens, so that we can return to main class
to go into switch
*/
import java.util.*;

public class Human extends Texas{


  private final ArrayList<Card> river = new ArrayList<Card>();
  private ArrayList<Card> cards = new ArrayList<Card>();
  private ArrayList<Card> TotalHumanHand = new ArrayList<Card>();
  private Stack<Card> pile = new Stack<Card>();
  private static Deck deck =  new Deck();

  public void Human(){
    
  } //constructor
  public final void makeHand(ArrayList<Card> river) {
    pile = deck.getPile();

    Card card1 = pile.pop();
    Card card2 = pile.pop();

    cards.add(card1);
    cards.add(card2);
    deck.getRiver();

    for(int i = 0; i < 2; i++){
      Card card = cards.get(i);
      TotalHumanHand.add(card);
    }
    for(int i = 0; i < 5; i++){
      Card card = river.get(i);
      TotalHumanHand.add(card);
    }

    for(int i = 0; i < TotalHumanHand.size(); i++){
      int index = i;
      int small = TotalHumanHand.get(i).intValue();
      for(int j = i+1; j < TotalHumanHand.size(); j++){
        if(small > TotalHumanHand.get(j).intValue()){
          small = TotalHumanHand.get(j).intValue();
          index = j;
        }// if
      }//for
      if (small == TotalHumanHand.get(i).intValue()){
          //nothing
      } else{
        int temp = TotalHumanHand.get(i).intValue();
        TotalHumanHand.set(i, TotalHumanHand.get(small));
        TotalHumanHand.set(small, TotalHumanHand.get(i));
      }
    } // for
  } // make hand
  public ArrayList<Card> getHumanHand(){
    return cards;
  }// just the humans hand

public ArrayList<Card> getTotalHumanHand(){
  return TotalHumanHand;
} // all cards for the human

public int isPaired(){
  int count = 0;
  int pair = 5;
  int full = 7;
if (TotalHumanHand != null) {
  for (int i = 0; i < 7; i++){
    for (int j = i + 1; j < 7; j++){
      if (TotalHumanHand.get(i) == TotalHumanHand.get(j)){
        count++;
      } // adds one if it sees a repeated card
      if (count == 1){
        for (int k = i + 1; k < 7; k++){
          if (TotalHumanHand.get(i) == TotalHumanHand.get(k) && TotalHumanHand.get(k) != TotalHumanHand.get(j)){
            pair++;
          }
            return pair;
        } // returns two pair
        return count;
      } // if count = 1, there is one pair
      if (count == 2){
        for (int k = i + 1; k < 7; k++){
          if (TotalHumanHand.get(i) == TotalHumanHand.get(k) && TotalHumanHand.get(k) != TotalHumanHand.get(j)){
            full++;
          }
            return full;
        } // returns full house
        return count;
      } // if count = 2, sees card 2 other times, is three of a kind
      if (count == 3){
        return count;
      } // if count = 3, sees card 3 other times, is four of a kind
    }
  }
}
return count;
} // checks to see what pairs there are

public boolean isSameSuit(){
  while (TotalHumanHand != null) {
  for (int i = 0; i < 7; i++){
    String suit = TotalHumanHand.get(i).getSuit();
    int c = 0;
    int d = 0;
    int h = 0;
    int s = 0;
    if (suit.equals("Clubs")){
      c++;
    } if (suit.equals("Diamonds")){
      d++;
    } if (suit.equals("Hearts")){
      h++;
    } if (suit.equals("Suits")){
      s++;
    }
    while (c == 5 || d == 5 || h == 5 || s == 5){
      return true;
    }
  }
}
return false;
}

public boolean isStraight(){
int counter = 0;
if(TotalHumanHand != null) {
  for (int i = 0; i < 7; i++){

    if (TotalHumanHand.get(i).intValue() == TotalHumanHand.get(i-1).intValue()){
      continue;
    } if (TotalHumanHand.get(i).intValue() == TotalHumanHand.get(i).intValue() - 1){
      counter++;
    } else {
      return false;
    }
  }
}
  if (counter >= 5){
    return true;
  }
  else{
    return false;
  }
} // checks to see if there is a straight in the 7 cards


public int evaluate(){
int HumanHand = 0;

for (int i = 0; i < 7; i++) {

  if (isPaired() >= 1) {
    if (isPaired() >= 2) {
      if (isPaired() == 3) {
        HumanHand = 8;
      } // Four of a Kind

      else if (isPaired() == 8) {
        HumanHand = 7;
      } //Full House

      else if (isPaired() == 2){
        HumanHand = 4;
    } // Three of a Kind

    else if (isPaired() == 6) {
      HumanHand = 3;
    } // Two Pair

    else if (isPaired() == 1){
      HumanHand = 2;
    } // Pair
  }
} // Paired Hands
if (isSameSuit() == true) {
  if (isStraight() == true) {
    if ((TotalHumanHand.get(0).intValue() == 14) || (TotalHumanHand.get(1).intValue() == 14) || (TotalHumanHand.get(2).intValue() == 14)){
      HumanHand = 10;
    } // Royal Flush

    else {
      HumanHand = 9;
    } // Striaght Flush
  }

  else {
    HumanHand = 6;
  } // Flush
}

if (isStraight() == true) {
  HumanHand = 5;
} // Straight

  else {
    HumanHand = 1;
  } // High Card
}
return HumanHand;
}


}// human class

//>>>>>>>>>>>>>>>>>>>>>>>>>>>
//Here it should belone to HumanHand, and what it returns, will be passed into texes
//And that number will be used in comparing
