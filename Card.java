import java.util.*;

public class Card extends Texas{

    private int value;
    private int suit;

    private static String[] suits = {"Hearts", "Spades", "Diamonds", "Clubs" };
    private static String[] values  = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};


  public Card(int values, int suits){
      this.value = values;
      this.suit = suits;
  } // constructor for a card, gives values to JQKA etc

  public String toString(){
    String card = value() + " of " + getSuit();
    return card;
  }// returns the card as a string


  public String value() {
    String result = "";

    if (value >= 0 && value <= 8){
      result = Integer.toString(value+2);
    }
    else if (value >= 9 && value <= 12){
      if (value == 9){
        result += "J";
      } else if (value == 10) {
        result += "Q";
      } else if (value == 11) {
        result += "K";
      } else if (value == 12) {
        result += "A";
      } else {
        result.equals("ERROR NOT A CARD");
      }
    }
    return result;
  }// gets the value of a card

  public String getSuit(){
    String result = "";
    if (suit == 0){
      result += "Hearts";
    } else if (suit == 1){
      result += "Spades";
    }else if (suit == 2){
      result += "Diamonds";
    }else if (suit == 3){
      result += "Clubs";
    }else{
      result.equals("ERROR NOT A SUIT");
    }
    return result;
  }// get suit method

  public int intValue(){
    return value;
  } // gets pure numerical value (2-14)

  public int suit(){
    return suit;
  }// gets suit as a string




}// card class
