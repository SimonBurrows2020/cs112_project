import java.util.*;

public class Deck extends Texas{

  private ArrayList<Card> deck;
  private Stack<Card> pile;
  private ArrayList<Card> river = new ArrayList<Card>();

  public Deck(){
    deck = new ArrayList<Card>();
    pile = new Stack<Card>();
    String pil = "";
    for (int i = 0; i < 4; i++){
      for (int j = 0; j < 13; j++){
        deck.add(new Card(j,i));
      }
    }// creates deck
    for (int i = 0; i < deck.size(); i++){
      pile.push(deck.get(i));
      pil += pile.toString();
    } // adds to pile of cards
    Collections.shuffle(pile);
  } // constructor to create a perfect deck, adds to a stack


  public Stack<Card> getPile(){
    return pile;
  }// gets pile

  public Card draw(){

    Card a = pile.pop();
    return a;

  }// draws a specified amount of cards

  public ArrayList<Card> cards(){
    return deck;
  }//gets the arraylist of cards

  public String toString(){
    String result = "";
    for (int i = 0; i < 52; i++){
      result += deck.get(i).value() + " of " + deck.get(i).getSuit();
    }
    return result;
  } // converts the deck into an array for reading


  public final void setRiver(){
    //Deck thePile = new Deck();

    Card card5 = draw();
    Card card6 = draw();
    Card card7 = draw();
    Card card8 = draw();
    Card card9 = draw();

    river.add(card5);
    river.add(card6);
    river.add(card7);
    river.add(card8);
    river.add(card9);
    river = river;
  }// sets the river

  public ArrayList<Card> getRiver(){
    return river;
  }
}// Deck class
