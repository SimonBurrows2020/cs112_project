import java.util.*;

public class Texas{
  private static Deck deck =  new Deck();


  public static void main(String[] args) {
    int userScore = 0;
    int computerScore = 0;
    Human HumanPlayer = new Human();
    Computer ComputerPlayer = new Computer();
    System.out.println("Hello! Welcome to the world of almost Texas Hold'em!");
    Stack<Card> pile = deck.getPile();

 if (playAgain(HumanPlayer,ComputerPlayer) == true) {
   playGame(HumanPlayer,ComputerPlayer);
 }
 else {
   System.out.println("Thanks for playing! Final score is User: " + userScore + " and Computer: "+ computerScore);
 }

  }// main
  public static boolean playAgain(Human hPlayer, Computer cPlayer){
    Scanner scan = new Scanner(System.in);

    System.out.println("Do you want to play? YES or NO");

    String userChoice = scan.nextLine().toUpperCase();
    if (userChoice.equals("YES")){
      return true;
    } else if (userChoice.equals("NO")){

      return false;
    } else{
      System.out.println("Please try again.");
    }
    return false;
  } // Play round method

  public static void playGame(Human hPlayer, Computer cPlayer){


    deck.setRiver();
    hPlayer.makeHand(deck.getRiver());
    cPlayer.makeHand(deck.getRiver());
    Scanner scan = new Scanner(System.in);


    System.out.println("Your current cards are : " + hPlayer.getHumanHand());
    System.out.println("The current river is : " + deck.getRiver());
    System.out.println("Do you want to play or fold?");
    String userChoice = scan.nextLine().toUpperCase();

    if(userChoice.equals("PLAY")){
      findWinner(hPlayer,cPlayer);
    } else if (userChoice.equals("FOLD")){
      playAgain(hPlayer, cPlayer);
    } else {
      System.out.println("Please enter a valid response.");

    }

  } // playgame method

  public static void findWinner(Human HumanHand, Computer ComputerHand){

    ArrayList<Card> TotalHumanHand = new ArrayList<Card>();
    ArrayList<Card> TotalComputerHand = new ArrayList<Card>();
    int userScore = 0;
    int computerScore = 0;

    int winner = 0;
    System.out.println("Evaluating...");

    if (HumanHand.evaluate() > ComputerHand.evaluate())
    {
      userScore++;
      winner = 1;
    }

    else if (HumanHand.evaluate() < ComputerHand.evaluate())
    {
      computerScore++;
      winner = 2;
    }

    else if (HumanHand.evaluate() == ComputerHand.evaluate()) {

      for (int i = 0; i < 7; i++){
        TotalHumanHand = HumanHand.getTotalHumanHand();
        TotalComputerHand = ComputerHand.getTotalComputerHand();
        if (TotalHumanHand.get(i).intValue() == TotalComputerHand.get(i).intValue()){
            winner = 3;
        }
        else if (TotalHumanHand.get(i).intValue() > TotalComputerHand.get(i).intValue()){
            userScore++;
            winner = 1;
        }
        else if (TotalComputerHand.get(i).intValue() > TotalHumanHand.get(i).intValue()){
            computerScore++;
            winner = 2;
        }
      }
    }

    if (winner == 1){
      System.out.println("Human wins!");
    } else if (winner == 2){
      System.out.println("Computer wins!");
      System.out.println("Their hand was: " + ComputerHand.getComputerHand());
    } else if (winner == 3){
      System.out.println("You tied! Somehow....");
    } else {
      System.out.println("What the hell did you do?!");
    }
  }// winner class
}//Texas class
