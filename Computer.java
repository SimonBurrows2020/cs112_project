import java.util.*;

public class Computer extends Texas{


  private final ArrayList<Card> river = new ArrayList<Card>();
  private ArrayList<Card> cards = new ArrayList<Card>();
  private ArrayList<Card> TotalComputerHand = new ArrayList<Card>();
  private Stack<Card> pile = new Stack<Card>();
  private static Deck deck =  new Deck();

  public void Computer(){

  } // constructor
  public final void makeHand(ArrayList<Card> river){
    Deck stack = new Deck();
    pile = stack.getPile();

    Card card1 = pile.pop();
    Card card2 = pile.pop();

    cards.add(card1);
    cards.add(card2);
    deck.getRiver();

    for(int i = 0; i < 2; i++){
      TotalComputerHand.add(cards.get(i));
    }
    for(int i = 0; i < 5; i++){
      TotalComputerHand.add(river.get(i));
    }

    for(int i = 0; i < TotalComputerHand.size(); i++){
      int index = i;
      int small = TotalComputerHand.get(i).intValue();
      for(int j = i+1; j < TotalComputerHand.size(); j++){
        if(small > TotalComputerHand.get(j).intValue()){
          small = TotalComputerHand.get(j).intValue();
          index = j;
        }// if
      }//for
      if (small == TotalComputerHand.get(i).intValue()){
          //nothing
      } else{
        int temp = TotalComputerHand.get(i).intValue();
        TotalComputerHand.set(i, TotalComputerHand.get(small));
        TotalComputerHand.set(small, TotalComputerHand.get(i));
      }
    } // for
  }
public ArrayList<Card> getComputerHand() {
  return cards;
}
public ArrayList<Card> getTotalComputerHand(){
  return TotalComputerHand;
}

public int isPaired(){
  int count = 0;
  int pair = 5;
  int full = 7;
if (TotalComputerHand != null) {
  for (int i = 0; i < 7; i++){
    for (int j = i + 1; j < 7; j++){
      if (TotalComputerHand.get(i) == TotalComputerHand.get(j)){
        count++;
      } // adds one if it sees a repeated card
      if (count == 1){
        for (int k = i + 1; k < 7; k++){
          if (TotalComputerHand.get(i) == TotalComputerHand.get(k) && TotalComputerHand.get(k) != TotalComputerHand.get(j)){
            pair++;
          }
            return pair;
        } // returns two pair
        return count;
      } // if count = 1, there is one pair
      if (count == 2){
        for (int k = i + 1; k < 7; k++){
          if (TotalComputerHand.get(i) == TotalComputerHand.get(k) && TotalComputerHand.get(k) != TotalComputerHand.get(j)){
            full++;
          }
            return full;
        } // returns full house
        return count;
      } // if count = 2, sees card 2 other times, is three of a kind
      if (count == 3){
        return count;
      } // if count = 3, sees card 3 other times, is four of a kind
    }
  }
}
return count;
} // checks to see what pairs there are

public boolean isSameSuit(){
  while (TotalComputerHand != null) {
  for (int i = 0; i < 7; i++){
    String suit = TotalComputerHand.get(i).getSuit();
    int c = 0;
    int d = 0;
    int h = 0;
    int s = 0;
    if (suit.equals("Clubs")){
      c++;
    } if (suit.equals("Diamonds")){
      d++;
    } if (suit.equals("Hearts")){
      h++;
    } if (suit.equals("Suits")){
      s++;
    }
    while (c == 5 || d == 5 || h == 5 || s == 5){
      return true;
    }
  }
}
return false;
}

public boolean isStraight(){
int counter = 0;
if(TotalComputerHand != null) {
  for (int i = 0; i < 7; i++){

    if (TotalComputerHand.get(i).intValue() == TotalComputerHand.get(i-1).intValue()){
      continue;
    } if (TotalComputerHand.get(i).intValue() == TotalComputerHand.get(i).intValue() - 1){
      counter++;
    } else {
      return false;
    }
  }
}
  if (counter >= 5){
    return true;
  }
  else{
    return false;
  }
} // checks to see if there is a straight in the 7 cards


public int evaluate(){
int ComputerHand = 0;

for (int i = 0; i < 7; i++) {

  if (isPaired() >= 1) {
    if (isPaired() >= 2) {
      if (isPaired() == 3) {
        ComputerHand = 8;
      } // Four of a Kind

      else if (isPaired() == 8) {
        ComputerHand = 7;
      } //Full House

      else if (isPaired() == 2){
        ComputerHand = 4;
    } // Three of a Kind

    else if (isPaired() == 6) {
      ComputerHand = 3;
    } // Two Pair

    else if (isPaired() == 1){
      ComputerHand = 2;
    } // Pair
  }
} // Paired Hands
if (isSameSuit() == true) {
  if (isStraight() == true) {
    if ((TotalComputerHand.get(0).intValue() == 14) || (TotalComputerHand.get(1).intValue() == 14) || (TotalComputerHand.get(2).intValue() == 14)){
      ComputerHand = 10;
    } // Royal Flush

    else {
      ComputerHand = 9;
    } // Striaght Flush
  }

  else {
    ComputerHand = 6;
  } // Flush
}

if (isStraight() == true) {
  ComputerHand = 5;
} // Straight

  else {
    ComputerHand = 1;
  } // High Card
}
return ComputerHand;
}


}// Computer class

//>>>>>>>>>>>>>>>>>>>>>>>>>>>
//Here it should belone to HumanHand, and what it returns, will be passed into texes
//And that number will be used in comparing
